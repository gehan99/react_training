import React from "react";
import "../../App.css"

export default function DisplayData() {
    return (
        React.createElement("div", null, React.createElement("h1", null, "Welcome to Virtusa"),
            React.createElement("div", null, React.createElement("h2", { className: 'bluecolor' }, "React training programm")),
            React.createElement("div", null, React.createElement("h3", { className: 'redcolor' }, "Conducted by Learning Delivery Organization")))
    )
}