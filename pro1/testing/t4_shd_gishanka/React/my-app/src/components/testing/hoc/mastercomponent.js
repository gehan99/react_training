/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react';

const mastercomponent = (OriginalComponent,initialValue) => {

    function Newcomponent(props) {
        const [counter, setCounter] = useState(initialValue);
        const doHandleIncrement = () => {
            setCounter(counter => counter + 1)
        }
        return (
            <div>
                <OriginalComponent
                    counter={counter}
                    onHandleIncrement={doHandleIncrement} />
            </div>
        )
    }

    return Newcomponent;
}
export default mastercomponent;