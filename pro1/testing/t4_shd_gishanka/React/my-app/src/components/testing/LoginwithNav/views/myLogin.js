import React, { useState, } from 'react'
import {useNavigate} from 'react-router-dom'

export default function myLogin() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [value,setvalue]=useState("")
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const nav = useNavigate();


  const onHandleSubmit=(e)=>{
    if(value.trim()===""){
      alert("invalid data...");
      return;
    }

    
    e.preventDefult();
    sessionStorage.setItem("auth",value)
    setTimeout(()=>{
      nav('/dashboard')
    },5000)
  }

  const onHandleChange=(event)=>{
    setvalue(event.target.value);
  }
  return (
    <div>myLogin
      <form onSubmit={onHandleSubmit}>
        <input type="text" value={value}onChange={onHandleChange}/>
        <button type='submit'>Login</button>
      </form>
    </div>
    
  )
}
