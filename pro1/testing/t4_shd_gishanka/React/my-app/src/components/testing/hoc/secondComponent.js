import React from 'react'
import mastercomponent from './mastercomponent'

function secondComponent(props) {
  return (
    <div onMouseOver={props.onHandleIncrement}>
        this is handled by HOC {props.counter}
        <p style={{'fontSize':props.counter}} 
        >click the text</p>
    </div>
  )
}

export default mastercomponent(secondComponent,10)