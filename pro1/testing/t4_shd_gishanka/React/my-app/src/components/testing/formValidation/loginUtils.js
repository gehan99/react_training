import * as Yup from "yup"
export const initialValue={
    email:'',
    password:''
}

export const loginSchema=Yup.object().shape({
    email:Yup.string().email().required("Email is mandatory "),
    password:Yup.string().required("Password is mandotory ").min(3,"password is too short")
})

export const submitForm = (values)=>{
    console.log(values);
}
