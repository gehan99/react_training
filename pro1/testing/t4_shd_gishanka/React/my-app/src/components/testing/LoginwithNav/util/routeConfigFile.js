import { useRoutes } from 'react-router-dom'
import MyLogin from '../views/myLogin'
import MyAdmin from '../views/myAdmin'
import MyDashboard from '../views/myDashboard'
// import MyAboutUs from '../views/myAboutUs'
import MyPagenotFount from '../views/myPagenotFount'
import ProtectedRoute from '../views/protectedRoute'
const RouteConfig = () => {
    let data = {
        loggedin: false,
        name: "virtusa"
    };

    let routes = useRoutes([
        { path: '/', element: <MyLogin /> },
        { path: '/login', element: <MyLogin /> },

        { path: '/dashboard', element: <MyDashboard /> },
        // {path:'/aboutus',element:<MyAboutUs/>},
        { path: '*', element: <MyPagenotFount /> },
        {
            path: '/admin', element: (
                <ProtectedRoute feed={data}>
                    <MyAdmin />
                </ProtectedRoute>
            )
        }
    ]);

    return routes;
}

export default RouteConfig;

