import React from 'react';
import { Field, Form, Formik } from 'formik'
import { initialValue, loginSchema, submitForm } from './loginUtils';

export default function login() {
    return (
        <>
        <h5>ggggggg</h5>
            <Formik
                initialValues={initialValue}
                validationSchema={loginSchema}
                onSubmit={submitForm}
            >
                {
                    (formik) => {
                        const { errors, touched,dirty,isValid } = formik;
                       return( <Form>
                            <div>
                                <label>username</label>
                                <Field
                                    type="email"
                                    name="email"
                                    id="email"
                                    className={
                                        errors.email && touched.email ? "input-error" : null
                                    }
                                />
                            </div>
                            <div>
                                <label>password</label>
                                <Field
                                    type="password"
                                    name="password"
                                    id="password"
                                    className={
                                        errors.password && touched.password ? "input-error" : null
                                    }
                                />
                            </div>
                    
                    <button
                                type='submit'
                                className={!(dirty && isValid) ? "disabled-btn" : ""}
                                disabled={!(dirty && isValid)}
                            >
                                Sign In

                            </button>
                            </Form>
                       );
                    }
                }
                
            </Formik>
        </>
    );
}
