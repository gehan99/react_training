import React from 'react'
import mastercomponent from './mastercomponent'

function firstComponent(props) {
  return (
    <div onClick={props.onHandleIncrement}>
        this is handled by HOC {props.counter}
        <p style={{'fontSize':props.counter}} 
        >click the text</p>
    </div>
  )
}

export default mastercomponent(firstComponent,5)