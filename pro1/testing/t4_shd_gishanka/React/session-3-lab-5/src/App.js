import AppNavBar from './components/views/appNavBar';
import {BrowserRouter as Router } from 'react-router-dom';
import RouteConfig from './components/util/routeConfigfile';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';


function App() {
  return (
    <div>
      {/* <AppNavBar/> */}
      <Router>
        <nav>
          {/* <Link to = "/">Login</Link> */}
          {/* <Link to = "/admin">Admin</Link>
          <Link to = "/dashboard">dashboard</Link>
          <Link to = "/helpPage">HelpPage</Link> */}
          <AppNavBar/>
          <RouteConfig/>
        </nav>
      </Router>

    </div>
  );
}

export default App;
