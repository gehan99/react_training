import { useRoutes } from 'react-router-dom'
import Admin from '../views/admin';
import Dashboard from '../views/dashboard';
import HelpPage from '../views/helppage';
import LoginPage from '../views/loginPage';
import PageNotFound from '../views/pageNotfound';
import ProtectedRoute from '../views/protectedRoute';

const RouteConfig = () => {
    let data = {
        loggedin: false,
        name: "gehan"
    };
    let routes = useRoutes([
        { path: '/', element: <LoginPage /> },
        { path: '/login', element: <LoginPage /> },
        {path: '/admin', element: (
            <PageNotFound feed={data}>
                <Admin />
            </PageNotFound>
        ) },
        {
            path: '/dashboard', element: (
            <ProtectedRoute feed={data}>
                <Dashboard />
            </ProtectedRoute>)
        },
        {
            path: '/helpPage', element: (
                <ProtectedRoute feed={data}>
                    <HelpPage />
                </ProtectedRoute>)
        },

        { path: '*', element: <PageNotFound /> },
      

    ])
    return routes;
}

export default RouteConfig;