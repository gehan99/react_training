import React, { useState } from 'react';
import '../../App.css';
import { useNavigate} from 'react-router-dom'
// import { Card, Form, Button } from 'react-bootstrap';

export default function loginPage() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const[Uservalue,setUserValue]=useState("");
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const nav = useNavigate();
 
  let data = {
    name:Uservalue,
    role:(Uservalue ==='gehan')?'admin':'employee'
  }


 const onHandleSubmit=(e)=>{
    // if(Uservalue.trim()===""){
    //   alert("Invalid Data");
    //   return;
    // }
     e.preventDefault();
     sessionStorage.setItem("auth",JSON.stringify(data));
     setTimeout(()=>{
      nav('/dashboard')
     },5000)
  }

  const onHandleChange=(event)=>{
    setUserValue(event.target.value)
    console.log(Uservalue)
  }
  
  return (
    <div className='container list'>
      <div>
        <form onSubmit={onHandleSubmit}>
          <div>
            <label>User Name</label>
            <div><input type="text" name="name" value={Uservalue} onChange={onHandleChange}/></div>
          </div>
          <br/>
          <div>
            <button type='submit'>submit</button>
          </div>
        </form>
      </div>
      <br />
    </div>
  )
}
