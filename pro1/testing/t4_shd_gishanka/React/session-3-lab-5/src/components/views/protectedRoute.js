import React from 'react'
import { Navigate } from 'react-router-dom';

export default function protectedRoute(props) {
    // let canshow = props.feed.loggedin;
    let data = JSON.parse(sessionStorage.getItem("auth"));

    console.log("canshow",data);

    if(data.name === "gehan" || data.role ==="admin"){
        return props.children;
    }else{
        return <Navigate to = "/login" replace/>;
    }

}

// export default function ProtectedRouteRolebased(props){


// }
