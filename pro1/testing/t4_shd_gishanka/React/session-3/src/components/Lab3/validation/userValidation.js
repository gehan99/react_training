import * as yup from 'yup';

export const userSchema = yup.object().shape({
    fname:yup.string().required("Required"),
    lname:yup.string().required("Required"),
    uname:yup.string().required("Required"),
    address:yup.string().required("Required"),
    email:yup.string().email("please enter valied email ").required("Required"),
    mobile:yup.string().min(1).max(10).required("Required"),
})