import React, { useState } from 'react'
import {userSchema} from './validation/userValidation'

export default function userRegisterFormWithYup() {
    const initialValues = { fname: "", lname: "", uname: "", address: "", email: "", mobile: "" }
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const[usersValue, setUserValue] = useState(initialValues);
    
    const doHandleSubmit = (e) =>{
        const { name, value } = e.target;
        setUserValue({ ...usersValue, [name]: value });

    }

    const submitdata= async (e)=>{
        e.preventDefault();
        console.log("cliked submit button" + usersValue);

        const isValid = await userSchema.isValid(usersValue);
        console.log(isValid);
    }


    return (
        <div>
            <h3> UserRegisterFormWithYup lab -3</h3>
            <div className='container'>
                <pre></pre>
                <form onSubmit={submitdata}>
                    <div>
                        <label>Fname</label>
                        <div><input type='text'  name="fname" placeholder='Enter your First name' value={usersValue.fname} onChange={doHandleSubmit}/></div>
                    </div>
                    <div>
                        <label>Lname</label>
                        <div><input type='text' name='lname' placeholder='Enter your Last name' value={usersValue.lname} onChange={doHandleSubmit} /></div>
                    </div>
                    <div>
                        <label>Uname</label>
                        <div><input type='text' name='uname' placeholder='Enter your User name' value={usersValue.uname} onChange={doHandleSubmit}/></div>
                    </div>
                    <div>
                        <label>Address</label>
                        <div><input type='text' name='address' placeholder='Enter your address' value={usersValue.address} onChange={doHandleSubmit}/></div>
                    </div>
                    <div>
                        <label>Email</label>
                        <div><input type='text' name='email' placeholder='Enter your Email.' value={usersValue.email} onChange={doHandleSubmit} /></div>
                    </div>
                    <div>
                        <label>Mobile Number</label>
                        <div><input type='text' placeholder='Enter your mobile number' value={usersValue.mobile} onChange={doHandleSubmit}/></div>
                    </div>
                    <br />
                    <div>
                        <div><button type='submit'>Submit</button></div>
                    </div>
                </form>
            </div>
        </div>
    )
}
