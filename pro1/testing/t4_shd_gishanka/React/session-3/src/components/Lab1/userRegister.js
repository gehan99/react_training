import React, { useEffect, useState } from 'react';
import '../../App.css';
import UserDetails from '../Lab2/userDetails';
import UserRegisterFormWithYup from '../Lab3/userRegisterFormWithYup';


export default function userRegister() {
    const initialValues = { fname: "", lname: "", uname: "", address: "", email: "", mobile: "" }
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [Uvalue, setUvalue] = useState(initialValues);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [userFormErrors, setUserFormErrors] = useState({});
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [isSubmit, setIsSubmit] = useState(false);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [submitedDataset,setSubmitedData]=useState({})

    const doHandleLoginChange = (e) => {
        console.log(e.target);
        const { name, value } = e.target;
        setUvalue({ ...Uvalue, [name]: value });
        

    }

    const submitData = (e) => {
        e.preventDefault();
        console.log("cliked submit button" + Uvalue);
        setUserFormErrors(FormValidationSection(Uvalue));
        setIsSubmit(true);
    };

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        console.log(userFormErrors);
        if (Object.keys(userFormErrors).length === 0 && isSubmit) {
            console.log("submited data ",Uvalue);
            setSubmitedData(Uvalue)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userFormErrors])

    const FormValidationSection = (value) => {
        const errors = {};
        // const regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
        if (!Uvalue.fname) {
            errors.fname = " first name is required!";
        }
        if (!value.lname) {
            errors.lname = "last name cant be emapty!";
        }
        if (!value.uname) {
            errors.uname = "uname cant be empty!";
        }
        if (!value.address) {
            errors.address = "address cant be emapty!";
        }
        if (!value.email) {
            errors.email = "email cant be empty!";
        }
        if (!value.mobile) {
            errors.mobile = "mobile number cant be empty!"
           
        }
        return errors;
    }

    return (
        <div>
            <div className=' container row '>
                <div className='col-6'><div className='container'>
                    <div className='container'><h3>User Register lab-1</h3></div>
                    <form onSubmit={submitData}>
                        <div>
                            <label>Fname</label>
                            <div><input type="text" name="fname" onChange={doHandleLoginChange} value={Uvalue.fname} /></div>
                            <div className='validateComands'><p>{userFormErrors.fname}</p></div>
                        </div>
                        <div>
                            <label>Lname</label>
                            <div><input type="text" name="lname" onChange={doHandleLoginChange} value={Uvalue.lname} /></div>
                            <div className='validateComands'><p>{userFormErrors.lname}</p></div>
                        </div>
                        <div>
                            <label>Uname</label>
                            <div><input type="text" name="uname" onChange={doHandleLoginChange} value={Uvalue.uname} /></div>
                            <div className='validateComands'><p>{userFormErrors.uname}</p></div>
                        </div>
                        <div>
                            <label>Address</label>
                            <div><input type="text" name='address' onChange={doHandleLoginChange} value={Uvalue.address} /></div>
                            <div className='validateComands'><p>{userFormErrors.address}</p></div>
                        </div>
                        <div>
                            <label>Email</label>
                            <div><input type="text" name="email" onChange={doHandleLoginChange} value={Uvalue.email} /></div>
                            <div className='validateComands'><p >{userFormErrors.email}</p></div>
                        </div>
                        <div>
                            <label>Mobile Number</label>
                            <div><input type="number" name="mobile" onChange={doHandleLoginChange} value={Uvalue.mobile} /></div>
                            <div className='validateComands'><p>{userFormErrors.mobile}</p></div>
                        </div>
                        <div>
                            <div style={{ paddingTop: 15 }}> <button type='submit'>submit</button></div>
                        </div>
                    </form>
                    <br/>
                    <UserRegisterFormWithYup/>
                </div></div>
                <div className='col-6'>
                <UserDetails submiteddata={submitedDataset}/>
                </div>
            </div>


        </div>
    )
}
