import React from 'react'

export default function userDetails(props) {
    return (
        <div>
           
            <h3>User Details lab-2</h3>
            <div className='row'>
                <div className='col-6'>First Name:</div>
                <div className='col-6'>{props.submiteddata.fname}</div>
            </div>
            <div className='row'>
                <div className='col-6'>Last name:</div>
                <div className='col-6'>{props.submiteddata.lname}</div>
            </div>
            <div className='row'>
                <div className='col-6'>User name:</div>
                <div className='col-6'>{props.submiteddata.uname}</div>
            </div>

            <div className='row'>
                <div className='col-6'>Address:</div>
                <div className='col-6'>{props.submiteddata.address}</div>
            </div>
            <div className='row'>
                <div className='col-6'>Email:</div>
                <div className='col-6'>{props.submiteddata.email}</div>
            </div>
            <div className='row'>
                <div className='col-6'>Mobile:</div>
                <div className='col-6'>{props.submiteddata.mobile}</div>
            </div>
        </div>
    )
}
