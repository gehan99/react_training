import './App.css';
import { BrowserRouter as Router } from 'react-router-dom'
// import HomePage from './components/Views/homePage';
import AppNavbar from './components/NavBar/appNavbar';
import RouteConfig from './components/Utill/routeConfig';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div>
      <Router>
        <AppNavbar />
        <RouteConfig />
      </Router>
    </div>
  );
}

export default App;
