import {applyMiddleware,createStore,compose} from 'redux';
import thunk from 'redux-thunk';
import rootRe from '../reduces/rootReducer';
const middleware=[thunk]
const initialState={};


const store = createStore(
    rootRe,
    initialState,
    compose(
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION_ && window.__WEB_VITALS_POLYFILL__()
    )
)

export default store;