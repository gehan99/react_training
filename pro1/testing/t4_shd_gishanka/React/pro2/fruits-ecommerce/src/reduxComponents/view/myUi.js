import React from 'react'
import { getAlldata } from '../rdx/action/getalldata'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

function myUi() {
    return (
        <>
            <div>myUi</div>
            <div><button onClick={()=>getAlldata()} >Clickme</button></div>
        </>

    )
}
 function matchDispatchProps(dispatch){
    return bindActionCreators({fetchpost:getAlldata},dispatch)
 }

export default connect(null,matchDispatchProps)(myUi)