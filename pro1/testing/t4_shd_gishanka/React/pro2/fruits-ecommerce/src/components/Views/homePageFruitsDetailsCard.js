import React from 'react'
import { BsCartDash} from 'react-icons/bs';

export default function homePageFruitsDetailsCard(props) {
  return (
    <div>
        <div>
       <div className="card shadow  mb-5  rounded" style={{width: '15rem',boxShadow:10,borderColor:'orangered',backgroundColor:'orange',borderWidth:2}}>
        <img src={props.productImage}className="card-img-top" alt="..."  width={50} height={200}/>
        
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <h6 className="card-subtitle mb-2">Card subtitle</h6>
          <p className="card-text">of the card's content.22</p>
          {/* <button onClick={addValue}>+</button><a style={{paddingLeft:8,paddingRight:8}}>{numOfFriut}</a> <button onClick={reduce}>-</button> */}
         <div style={{paddingTop:10}}><button type="button" className="btn btn-primary"><BsCartDash/></button></div> 

         
        </div>
      </div>
      <br/>
    </div>
    </div>
  )
}
