/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-undef */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react'
import {useNavigate} from 'react-router-dom';
import {fetchUserdata} from '../Utill/httpUtils'

export default function homeheder_one() {

  const[uVal,setUval]=useState([])
  useEffect(()=>{
    loadData()
  },[setUval])

  const loadData=()=>{
    fetchUserdata().then(res=>{
      setUval(res.data);
    })
  }

  





  const nav= useNavigate()
  const gotoNextPage=()=>{
    nav('/fruits')
  }
  return (
    <div>
      {console.log(uVal)}
      <div className="card">
        <div className="card-body">
          <div className='row'>
            <div className='col-5'>
              <div style={{fontSize:40,fontWeight:'bold'}}><p>The Fresh and Organic <p style={{color:'green'}}>Fruits</p></p></div>
              <div className='text-left' style={{paddingTop:70}}><button type="button" className="btn btn-warning" onClick={gotoNextPage}>Shop Now..</button></div>
            </div>
            <div className='col-7 text-center'>
              <div className="has-bg-img center-block">
                <img className="bg-img" src="https://media.istockphoto.com/id/1395535082/photo/tropical-fruit-concept.jpg?b=1&s=612x612&w=0&k=20&c=OLzpk2tpYyUdQIZu6fdajsJPguna50W5383D6CxpEEc=" alt="..."  />
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}
