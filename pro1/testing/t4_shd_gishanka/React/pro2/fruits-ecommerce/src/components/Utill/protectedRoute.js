import React from 'react'
import { Navigate } from 'react-router-dom';
import secureLocalSorage from 'react-secure-storage'

export default function protectedRoute(props) {
    // let canshow = props.feed.loggedin;
    let data = JSON.parse(secureLocalSorage.getItem("auth"));

    if (data === null) {
        new Promise((resolve) => setTimeout(resolve, 500));
        return <Navigate to="/login" replace />
        
    } else {
        if (data.email && data.password && data.role)  {
            return props.children;
        }
    }

   

}
