import React from 'react'
import HomebodyoneCard from './homebodyoneCard'

export default function homebodyone() {
  const facility = [
    {
      id: 1,
      title: "Free Fast Delivery",
      description: "ddddddd"
    },
    {
      id: 2,
      title: "For Qulity Product",
      description: "ddddddd"
    },
    {
      id: 3,
      title: "Special Offer",
      description: "ddddddd"
    },
    {
      id: 4,
      title: "24 x 7 Support",
      description: "ddddddd"
    }
  ]
  return (
    <>
    <div style={{ paddingTop: 12 }}>
      <div className="card">
        <div className="card-body">
          <div className='row'>
            <div className='row'>
              {
                facility.map(u =>
                  <div className='col-3'>
                    <HomebodyoneCard 
                    facilityId={u.id}
                    facilityTitle={u.title}
                    facilityDescription={u.description}
                    />
                  </div>
                )
              }
            </div>

          </div>

        </div>
      </div>

    </div>
    </>
  )
}
