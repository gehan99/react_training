/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Footer from './footer';

export default function aboutUs() {
  return (
    <div >
      <div className='container-fluid' >
        <br />
        <div className="card shadow-sm p-3 mb-5 bg-white rounded" style={{backgroundColor:'lightgray'}} >
          <div className="card-body">
            <h2>About</h2>
            <div className='row'>
              <div className='col-7 '>
                <p>organic food, fresh or processed food produced by organic farming methods. Organic food is grown without the use of synthetic chemicals, such as human-made pesticides and fertilizers, and does not contain genetically modified organisms (GMOs). Organic foods include fresh produce, meats, and dairy products as well as processed foods such as crackers, drinks, and frozen meals. The market for organic food has grown significantly since the late 20th century, becoming a multibillion dollar industry with distinct production, processing, distribution, and retail systems.</p>
                <h4>Policy</h4>
                <p>Although organic food production began as an alternative farming method outside the mainstream, it eventually became divided between two distinct paths: (1) small-scale farms that may not be formally certified organic and thus depend on informed consumers who seek out local, fresh, organically grown foods; and (2) large-scale certified organic food (fresh and processed) that is typically transported large distances and is distributed through typical grocery store chains. If consumers know their local farmer and trust the farmer’s production methods, they may not demand a certification label. On the other hand, organic food produced far away and shipped is more likely to require a certification label to promote consumer trust and to prevent fraud, which exemplifies how national certification regulations are most beneficial.</p>
                <p>A regulatory framework is most important when consumers and farmers are geographically separated, and such a framework is likely to cater to larger-scale producers who participate in a more industrial system. This regulatory approach does not necessarily match consumers’ assumptions about organic food production, which typically include images of small family farms and the humane treatment of animals. In general, regulations surrounding organic food do not address more complex social concerns about family farms, farmworker wages, or farm size, and organic policy in some places does little to address animal welfare.</p>
                <p>Organic food policies were created largely to provide a certification system with specific rules regarding production methods, and only products that follow the guidelines are allowed to use the certified organic labels. In the United States, the Organic Foods Production Act of 1990 began the process of establishing enforceable rules to mandate how agricultural products are grown, sold, and labeled. The regulations concerning organic food and organic products are based on a National List of Allowed and Prohibited Substances, which is a critical aspect of certified organic farming methods. The United States Department of Agriculture (USDA) regulates organic production through its National Organic Program (NOP), which serves to facilitate national and international marketing and sales of organically produced food and to assure consumers that USDA certified organic products meet uniform standards. To this end, NOP established three specific labels for consumers on organic food products: “100% organic,” “organic,” or “made with organic ***,” which signify that a product’s ingredients are 100 percent, at least 95 percent, or 70 percent organic, respectively. Noncertified products cannot use the USDA organic seal, and violators face significant fines and penalties.</p>
              </div>
              <div className='col-5 text-center'>
                <div className="card " style={{ width: '18rem' }}>
                  <img className="card-img-top" src="https://www.westend61.de/images/0001010394pw/an-organic-fruit-farm-two-boys-picking-the-berry-fruits-from-the-bushes-MINF02799.jpg" alt="Card image cap" />
                </div>
                <br />
                <div className="card" style={{ width: '18rem' }}>
                  <img className="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmeee1cSmeWKfMCPe-loDxdcJr6iT_Dbw71w&usqp=CAU" alt="Card image cap" />
                </div>
                <br />
                <div className="card" style={{ width: '18rem' }}>
                  <img className="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUwiN0D02i5bKL_zk4ZtgdyFdtkv3VaMazfg&usqp=CAU" alt="Card image cap" />
                </div>
                <br />
                <div className="card" style={{ width: '18rem' }}>
                  <img className="card-img-top" src="https://agcamritsar.in/cam/fruit1.jpg" alt="Card image cap" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}
