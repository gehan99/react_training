import React from 'react'

export default function homebodyoneCard(props) {
  return (
    <div>
         <div className="card text-center shadow-lg p-3 mb-5  rounded" style={{width: '18rem',backgroundColor:"burlywood"}}>
        <div className="card-body">
          <h5 className="card-title">{props.facilityTitle}</h5>
          <h6 className="card-subtitle mb-2 text-muted">Card subtitle</h6>
          <p className="card-text">{props.facilityDescription}</p>
        </div>
      </div>
    </div>
  )
}
