/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
// import { BsCartDash} from 'react-icons/bs';

export default function footer() {
    return (
        <div style={{ paddingTop: 12 }}>
            <footer className="bg-light text-center text-lg-start">
                {/* Grid container */}
                <div className="container p-4">
                    {/*Grid row*/}
                    <div className="row">
                        {/*Grid column*/}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase">Quick Link</h5>
                            <ul className="list-unstyled mb-0">
                                <li>
                                    <a href="#!" className="text-dark">Home</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">About</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">Fruits</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">Blogs</a>
                                </li>
                            </ul>
                        </div>
                        {/*Grid column*/}
                        {/*Grid column*/}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase mb-0">Contact</h5>
                            {/* <ul className="list-unstyled"> */}
                                <li>
                                    <a>123 colombo, Sri Lanka</a>
                                </li>
                                <li>
                                    <a>+94 88 111 2956</a>
                                </li>
                                <li>
                                    <a>+94 89 111 2957</a>
                                </li>
                                <li>
                                    <a>abc.fruits@gmaol.com</a>
                                </li>
                            {/* </ul> */}
                        </div>
                        {/*Grid column*/}
                        {/*Grid column*/}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase">Follow us</h5>
                            <ul className="list-unstyled mb-0">
                                <li>
                                    <a href="#!" className="text-dark">Facebook</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">Twitter</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">Instagram</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">Google</a>
                                </li>
                            </ul>
                        </div>
                        {/*Grid column*/}
                        {/*Grid column*/}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase mb-0">Online payments</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!" className="text-dark">vissa</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-dark">paypal</a>
                                </li>
                            </ul>
                        </div>
                        {/*Grid column*/}
                    </div>
                    {/*Grid row*/}
                </div>
                {/* Grid container */}
                {/* Copyright */}
                <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                    © 2023 Copyright:
                    <a className="text-dark" href="https://mdbootstrap.com/">Fruit Ecommerce</a>
                </div>
                {/* Copyright */}
            </footer>
        </div>
    )
}
