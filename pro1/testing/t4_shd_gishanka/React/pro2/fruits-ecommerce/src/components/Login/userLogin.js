/* eslint-disable dot-location */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react'
import '../../App.css'
import { ToastContainer, toast } from 'react-toastify';
import { useFormik } from 'formik';
import { loginform } from '../Utill/validation';
import { useNavigate } from 'react-router-dom';
import { fetchUserdata } from '../Utill/httpUtils'
import { changeStatus } from '../Utill/test'
import secureLocalSorage from 'react-secure-storage'

export default function userLogin() {

  const notify = () => toast("success");
  const nav = useNavigate()
  const [allUsers, setAllUsers] = useState(false);
  const [selecteUser, setSelectedUser] = useState([]);


  const onSubmit = async (values, action) => {
    console.log("sssssssssssssssssssssss");
    console.log("get user data", values);
    await getUser(values);
    if (allUsers === true) {
      notify()
      secureLocalSorage.setItem("auth", JSON.stringify(selecteUser));
      await new Promise((resolve) => setTimeout(resolve, 1000));
      action.resetForm()
      console.log("22222d", allUsers);
      nav('/');


    } else {
      alert("User Name or password is incorect..")
      nav('/login')
    }
  }





  const getUser = async (v) => {
    console.log("get user function")
    await fetchUserdata().then((res) => {

      console.log("fetch data----")
      console.log(res)
      let ab = res.data
      console.log("asdes", ab)

      for (let index = 0; index < ab.length; index++) {
        let element = ab[index];
        if (element.password === v.password && element.email === v.email) {
          console.log("true----------user role=>", element.role);
          setSelectedUser(element)
          setAllUsers(true)
        } else {
          console.log('false');
        }
      }
    }
    )
  }



  const { values, errors, touched, isSubmitting, handleBlur, handleChange, handleSubmit } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },

    validationSchema: loginform,
    onSubmit
  });


  return (
    <div>
      {allUsers}
      <ToastContainer />
      <div className="card cardcenter shadow p-3 mb-5  rounded" style={{ width: '25rem' }}>
        <div className="card-body">
          <h5 className="card-title">Login</h5>

          <form onSubmit={handleSubmit} autoComplete="off">
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input
                value={values.email}
                onChange={handleChange}
                id='email'
                type="text"
                placeholder="Enter email"
                onBlur={handleBlur}
                className=
                "form-control"
              />
              {errors.email && touched.email && <p style={{ color: 'red' }}>{errors.email}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Password</label>
              <input
                value={values.uname}
                onChange={handleChange}
                id='password'
                type="password"
                placeholder="Enter password"
                onBlur={handleBlur}
                className=
                "form-control"
              />
              {errors.password && touched.password && <p style={{ color: 'red' }}>{errors.password}</p>}
            </div>
            <div style={{ paddingTop: 15, paddingBottom: 10, }}>Forgot Password?</div>
            <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
          </form>
        </div>
        {/* <div className='loginsubtitle'><h6 class="card-subtitle mb-2 ">Card subtitle</h6></div> */}
      </div>



    </div>
  )
}
