/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import {useNavigate} from 'react-router-dom'


export default function fruitcards(props) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const nav=useNavigate()
  const[numOfFriut,setNumOfFriut]= useState(0);
  const addValue=()=>{
    setNumOfFriut(numOfFriut+1)
  }
  const reduce =()=>{
      setNumOfFriut(numOfFriut-1)
  }

  const addTocart=()=>{
    nav('/shoppingCart')
  }



  return (
    <div>
       <div className="card shadow p-3 mb-5 bg-white rounded" style={{width: '18rem',boxShadow:10}}>
        <img src={props.image} className="card-img-top" alt="..."  width={50} height={200}/>
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <h6 className="card-subtitle mb-2">Card subtitle</h6>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <button onClick={addValue}>+</button><a style={{paddingLeft:8,paddingRight:8}}>{numOfFriut}</a> <button onClick={reduce}>-</button>
         <div style={{paddingTop:10}}><button type="button" className="btn btn-primary" onClick={addTocart}>Add</button></div> 
        </div>
      </div>
      <br/>
    </div>
  )
}
