import { useRoutes } from 'react-router-dom';
import UserLogin from '../Login/userLogin';
import ForgotPassword from '../Login/forgotPassword';
import UserRegister from '../Register/userRegister';
import HomePage from '../Views/homePage';
import AboutUs from '../Views/aboutUs';
import CartPage from '../Views/cartPage';
import HelpMe from '../Views/helpMe';
import NotFound from '../Views/notFound';
import FruitMarket from '../Views/fruitMarket';
import ProtectedRoute from './protectedRoute';
import UserProfile from '../Views/userProfile';
import Admin from '../Views/admin';

const RouteConfig = () => {
    let routes = useRoutes([
        { path: '/', element: <HomePage /> },
        { path: '/login', element: <UserLogin /> },
        { path: '/fogotpassword', element: <ForgotPassword /> },
        { path: '/userRegister', element: <UserRegister /> },
        { path: '/aboutUs', element: <AboutUs /> },
        {
            path: '/admin', element: (
                <ProtectedRoute>
                    <Admin />
                </ProtectedRoute>
            )
        },
        {
            path: '/shoppingCart', element:
                (<ProtectedRoute>
                    <CartPage />
                </ProtectedRoute>)
        },
        { path: '/helpMe', element: <HelpMe /> },
        { path: '/fruits', element: <FruitMarket /> },
        {
            path: '/userProfile', element:
                (<ProtectedRoute><UserProfile /></ProtectedRoute>)
        },
        { path: '*', element: <NotFound /> }
    ])
    return routes;
}

export default RouteConfig;