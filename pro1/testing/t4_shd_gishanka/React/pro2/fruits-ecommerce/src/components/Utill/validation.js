import * as yup from 'yup';

const userSchema = yup.object().shape({
    uname:yup.string().max(15,'must be 15 charaters or less').required("user name required*"),
    address:yup.string().max(20,'must be 20 charaters or less').required("address is required*"),
    mobile:yup.string().length(10,'invalid mobile number').required("mobile number is required*"),
    email:yup.string().email('email is invalid').required("Email is required*"),
    password:yup.string().min(6,'password must be atlest 6 characters').required("password is required*"),
    confirmPassword:yup.string().oneOf([yup.ref('password'),null],'password must match')
    .required("confirm password is required*"),
})

const loginform = yup.object().shape({
    email:yup.string().email('email is invalid').required("email is required*"),
    password:yup.string().min(6,'password must be atlest 6 characters').required("password is required*"),
})

export {userSchema,loginform};