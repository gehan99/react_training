import React from 'react'

export default function cartPage() {
  return (
    <div>
      <table className="table container-fluid text-center">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">image</th>
            <th scope="col">name</th>
            <th scope="col">Description</th>
            <th scope="col">Qty</th>
            <th scope="col">Price</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>Mark</td>
            <td>Otto</td>
            <td><button type="button" className="btn btn-danger">Delete</button></td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td>Mark</td>
            <td>Otto</td>
            <td><button type="button" className="btn btn-danger">Delete</button></td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
            <td>Mark</td>
            <td>Otto</td>
            <td><button type="button" className="btn btn-danger">Delete</button></td>
          </tr>
        </tbody>
      </table>
      
      <div className='row  float-right'>
        <div className='col-9'></div>
        <div className='col'>
          <label style={{ padding: 20 }}>Rs</label>
          <label>12300.00</label>
        </div>
      </div>
      <div className='row'>
      <div className='col-9'></div>
        <div className='col' style={{ paddingLeft: 30 }}>
          <button type="button" className="btn btn-primary">Pay</button>
          <button type="button" className="btn btn-primary">Cancel</button>
        </div>
      </div>


      {/* <div className='row'>
        <div className='col-11'>Total:</div>
        <div className='col'>Rs 1000.00</div>
      </div>

      <div className='row' >
        <div className='col-11'> <button type="button" className="btn btn-primary">Pay</button></div>
        <div className='col'> <button type="button" className="btn btn-primary">Cancel</button></div>
      </div> */}

    </div>
  )
}

// {orderlist.map((i, j) => {
//   return (
//       <tr>
//           <th scope="row">{j + 1}</th>
//           <td><img src={orderlist[j].p.productimageUrl} className="rounded" alt="..." /></td>
//           <td>{orderlist[j].p.pnumber}</td>
//           <td>{orderlist[j].p.description}</td>
//           <td> <div className="row">
//               <div className="col"><button type="button" className="btn btn-success" onClick={() => Decreasevalue(orderlist[j].id, orderlist[j].quntity)}>-</button></div>
//               <div className="col"><p>{orderlist[j].quntity}</p></div>
//               <div className="col"><button type="button" className="btn btn-success" onClick={() => IncreaseValue(orderlist[j].id, orderlist[j].quntity)}>+</button></div></div>
//           </td>
//           <td>Rs.{orderlist[j].quntity * orderlist[j].p.price}.00  <p >yyyy</p></td>
//           <td><button type="button" className="btn btn-danger" onClick={() => deteteAddedProducts(orderlist[j].id)}>Delete</button></td>
//       </tr>
//   )
// })}
