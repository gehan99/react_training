/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react'
import Footer from './footer'

export default function helpMe() {
  return (
    <div >
      <br />
      {/* <div className="card container-fluid"> */}
      {/* <div className="card-body"> */}
      <div className="container">
        <div className='row'>
          <div className='col-4'>

            <div className="card shadow p-3 mb-5 bg-white rounded" style={{ width: '18rem', backgroundColor: 'gray' }}>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Contact us</li>
                <li className="list-group-item">Live Chat</li>
                <li className="list-group-item">Email Us</li>
                <li className="list-group-item">Frequently Asked Questions</li>
              </ul>
            </div>
            <br />
            <div className="card shadow p-3 mb-5 bg-white rounded" style={{ width: '18rem', borderColor: 'gray' }}>
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Mobile Number</label>
                    <input type="mobile" className="form-control" id="exampleInputPassword1" placeholder="Mobile" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Popular Questions number</label>
                    <select className="form-control" id="exampleFormControlSelect1">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Question</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                  </div>

                  <br />
                  <button type="submit" className="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
          <div className='col-8'>      <div className="card mb-3 shadow p-3 mb-5 bg-white rounded">
            <img className="card-img-top " src="https://images.healthshots.com/healthshots/en/uploads/2022/04/17151621/fruit-salad-1600x900.jpg" alt="Card image cap" />
            <div className="card-body" style={{ color: 'blue', fontFamily: "monospace" }}>
              <h5 className="card-title" style={{ color: 'black' }}>Popular Questions from Other Buyers</h5>
              <p className="card-text">1.Can Made-in-China.com recommend specific products to me? </p>
              <p className="card-text">2.What should I do if I can't find suitable products? </p>
              <p className="card-text">3.How can I contact suppliers? </p>
              <p className="card-text">4.What's the difference between Gold Member, Diamond Member,Audited Supplier and License Verified? </p>
              <p className="card-text">5.How can I obtain more information about suppliers?</p>
              <p className="card-text">6.How can I reduce trade risks during business? </p>

              {/* <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p> */}
            </div>
          </div></div>
        </div>
      </div>
      {/* </div> */}
      {/* </div> */}

      <Footer />

    </div>
  )
}
