import React from 'react'
import Fruitcards from './fruitcards'
import {allFruitsDetails} from '../Utill/db'

export default function fruitMarket() {
  // const dataSet = ["1", "2", "3", "4", "5", "6", "7"]
  const dataSet = allFruitsDetails;

  return (
    <div className='container-fluid'>
      <br />
      <div className='row'>
          {
            dataSet.map(u => 
              <div className='col-3'>
                <Fruitcards image={u.img}/>
              </div>
              )
          }
{/* {
                        posts.map(post => <div className="col" key={post.id}><ItemCard
                            id={post.id}
                            image={post.productimageUrl}
                            pname={post.pname}
                            pnumber={post.pnumber}
                            description={post.description}
                            price={post.price}
                            warranty={post.warranty}
                            title={post.title}
                            subtitle={post.subtitle}
                            quntity={post.quntity}
                        /></div>)
                    } */}
      </div>
    </div>
  )
}
