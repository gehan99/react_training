/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react';
import { useFormik } from 'formik';
import {useNavigate} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import '../../App.css'
import { userSchema } from '../Utill/validation';
import {postUserData} from '../Utill/httpUtils'
// import Axios from 'axios'

export default function userregister() {
  
  const [uVal,setUval]= useState("")
  const nav = useNavigate();
  const notify = () => toast("Creating your account");

  const onSubmit = async(values,action) => {
    console.log("submited------>"+values);
    console.log("submited",values);
    console.log("submited-action",action);
    loadData(values)
    notify()
    await new Promise((resolve)=>setTimeout(resolve,5000));
    action.resetForm()
    nav('/login')

  }

  //with formik
  const { values, errors, touched,isSubmitting, handleBlur, handleChange, handleSubmit } = useFormik({
    initialValues: { uname: "", address: "", mobile: "", email: "", password: "", confirmPassword: "" },
    validationSchema: userSchema,
    
    onSubmit
  })
  console.log(errors);



 
  var loadData = (values)=>{
    console.log("submited data function--->",values.address);
    let newval={
      uname: values.uname, address: values.address, mobile:values.mobile, email:values.email, password:values.password,role:"user"
    }
    console.log("values------");
    console.log(newval);

    postUserData(newval).then(res=>setUval(res.data))

  }
  

  return (
    <div>
      {console.log("val",uVal)}
      <ToastContainer />
      <div className="card Registercardcenter shadow p-3 mb-5  rounded" style={{ width: '25rem' }}>
        <div className="card-body">
          <h5 className="card-title">Register</h5>
          <form onSubmit={handleSubmit} autoComplete="off" >
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">User Name</label>
              <input
                value={values.uname}
                onChange={handleChange}
                id='uname'
                type="text"
                placeholder="Enter uname"
                onBlur={handleBlur}
                className=
                "form-control"
              />
              {errors.uname && touched.uname && <p style={{ color: 'red' }}>{errors.uname}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Address</label>
              <input type="text"
                className="form-control"
                id='address'
                placeholder="Enter Address"
                value={values.address}
                onBlur={handleBlur}
                onChange={handleChange} />
              {errors.address && touched.address && <p style={{ color: 'red' }}>{errors.address}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Mobile Number</label>
              <input type="number"
                className="form-control"
                id='mobile'
                placeholder="Enter mobile number"
                value={values.mobile}
                onBlur={handleBlur}
                onChange={handleChange} />
              {errors.mobile && touched.mobile && <p style={{ color: 'red' }}>{errors.mobile}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input type="text"
                className="form-control"
                id='email'
                placeholder="Enter email"
                onBlur={handleBlur}
                value={values.email} onChange={handleChange} />
              {errors.email && touched.email && <p style={{ color: 'red' }}>{errors.email}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Password</label>
              <input type="password"
                className="form-control"
                id='password'
                onBlur={handleBlur}
                placeholder="Password"
                value={values.password} onChange={handleChange} />
              {errors.password && touched.password && <p style={{ color: 'red' }}>{errors.password}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Confirm Password</label>
              <input type="password"
                className="form-control"
                onBlur={handleBlur}
                id='confirmPassword'
                placeholder="Confirm Password"
                value={values.confirmPassword}
                onChange={handleChange} />
              {errors.confirmPassword && touched.confirmPassword && <p style={{ color: 'red' }}>{errors.confirmPassword}</p>}
            </div>
            <br />
            <div> <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button></div>
          </form>
        </div>
      </div>
    </div>
  )
}
