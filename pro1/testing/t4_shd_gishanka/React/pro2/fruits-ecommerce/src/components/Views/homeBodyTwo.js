/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import HomeBodyTwoCard from './homeBodyTwoCard'

export default function homeBodyTwo() {
    const offers = [
        {
            id: 1,
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSsJUv3Mb9SnDbERFTs0pFPlkbjaBFwiyPO3NC3KUiFUWAHT-rtgxw6gYZOo57njPdJoBk&usqp=CAU"
        },
        {
            id: 1,
            imageUrl: "https://png.pngtree.com/background/20210711/original/pngtree-mango-fruit-poster-banner-picture-image_1085341.jpg"
        }
    ]
    return (
        <div style={{ paddingTop: 12 }}>
            <div className="card" style={{paddingTop:50}}>
                <div className="card-body">
                    <div className='row'>
                        {
                            offers.map(u =>
                                <div className='col-6'>
                                    <HomeBodyTwoCard
                                        cardImage={u.imageUrl}
                                    />
                                </div>
                            )
                        }
                    </div>
                    <div className='text-center' style={{ paddingTop: 100 }} >
                        <br />
                        <h3>Vegetables and fruits are an important part of a healthy diet, and variety is as important as quantity.</h3>
                        <h3>No single fruit or vegetable provides all of the nutrients you need to be healthy. Eat plenty every day.</h3>
                        <div style={{paddingTop:50}}>
                            <p style={{ fontSize: 16, fontFamily: "inherit", fontWeight: "bold", color: "gray" }}>A diet rich in vegetables and fruits can lower blood pressure, reduce the risk of heart disease and stroke, prevent some types of cancer, lower risk of eye and digestive problems, and have a positive effect upon blood sugar, which can help keep appetite in check. Eating non-starchy vegetables and fruits like apples, pears, and green leafy vegetables may even promote weight loss. [1] Their low glycemic loads prevent blood sugar spikes that can increase hunger.

                                At least nine different families of fruits and vegetables exist, each with potentially hundreds of different plant compounds that are beneficial to health. Eat a variety of types and colors of produce in order to give your body the mix of nutrients it needs. This not only ensures a greater diversity of beneficial plant chemicals but also creates eye-appealing meals.</p></div>
                    </div>

                </div>
            </div>
        </div>
    )
}
