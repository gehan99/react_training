/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Navbar, Nav, Button } from 'react-bootstrap';
import { BsCartDash } from 'react-icons/bs';
import ProtectedRoute from '../Utill/protectedRoute';
import { valData, changeStatus } from '../Utill/test';
import secureLocalSorage from 'react-secure-storage'


export default function appNavbar() {
    
    // let statusCheck
    const[userType,setUsertype]=useState("");
    const [uname,setUname]=useState("");
    const nav = useNavigate();

    useEffect(() => {
        const a = ProtectedRoute;
        console.log(a);
        console.log("val-->", valData);
        let statusCheck = JSON.parse(secureLocalSorage.getItem("auth"));

        if (statusCheck === null) {
            setUsertype("")
            // return <Navigate to="/login" replace />
            
        } else {
            // if (statusCheck.email === "gehan@gmail.com" || statusCheck.password === "123456") {
            //     console.log("ddddd",statusCheck.role);
            //     setUsertype(statusCheck.role)
            // }
            console.log("ddddd",statusCheck.role);
            setUsertype(statusCheck.role)
            setUname(statusCheck.uname);

        }
    })

    const logout = () => {
        setUsertype(" ")
        secureLocalSorage.clear();
        // valData=0
        changeStatus(0)
        setTimeout(() => {
            
            nav('/login')
        }, 1000);
    }


    
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                {/* <Container> */}
                <Navbar.Brand as={Link} to="/">Fresh Fruits</Navbar.Brand>
                <Nav className="me-auto">
                {userType === "admin"? <Nav.Link as={Link} to="/admin">Admin</Nav.Link>:""} 
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    
                 <Nav.Link as={Link} to="/aboutUs">About</Nav.Link>
                    <Nav.Link as={Link} to="/helpMe">Help</Nav.Link>
                  
                </Nav>
                <Nav>
                    {userType === "user"||userType === "admin" ?  <Navbar.Brand >Hi {uname}</Navbar.Brand>:""}
                    {userType ===""?<Nav.Link as={Link} to="/login">Login</Nav.Link>:""}
                    {userType === ""?<Nav.Link as={Link} to="/userRegister">Register</Nav.Link>:""}
                    {userType === "user"? <Nav.Link as={Link} to="/userProfile">Profile</Nav.Link> : ""}
                    {userType === "user"||userType === "admin"? <Button bsStyle="black" style={{ backgroundColor: 'black' }} onClick={logout}>Logout</Button> : ""}
                    {/* <Nav.Link as={Link} to="/shoppingCart"> <BsCartDash size={30} color={'yellow'} /></Nav.Link> */}
                    {userType === "user" || userType === ""? <Nav.Link as={Link} to="/shoppingCart"> <BsCartDash size={30} color={'yellow'} /></Nav.Link>:""}
                </Nav>
                {/* </Container> */}
            </Navbar>
        </div>
    )
}
