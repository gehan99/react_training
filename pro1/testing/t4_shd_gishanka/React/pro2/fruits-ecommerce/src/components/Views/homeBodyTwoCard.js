/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react'

export default function homeBodyTwoCard(props) {
  return (
    <div>
      <div className="card shadow p-3 mb-5 bg-white rounded" style={{ width: '37rem', backgroundColor: 'orange' }}>
        {/* <div className="card-body"> */}
          <img className="card-img " src={props.cardImage} alt="Card image" />

        {/* </div> */}
      </div>
    </div>
  )
}
