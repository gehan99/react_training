import React from 'react'
import Footer from './footer';
import HomehederOne from './homeheder_one';
import HomeheaderTwo from './homeheader_two';
import Homebodyone from './homebodyone';
import HomeBodyTwo from './homeBodyTwo';

export default function homePage() {
  return (
    <div style={{backgroundColor:"black"}}>
      <HomehederOne/>
      <HomeBodyTwo/>
      <HomeheaderTwo/>
      <Homebodyone/>
      <Footer/>
    </div>
  )
}
