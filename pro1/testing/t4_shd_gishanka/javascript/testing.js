//arry forEach 
let num = [42, 51, 24, 98, 65, 12]

num.forEach((n) => {
    console.log(n);
})


console.log(num);

//n=data
//i=index
//number = array

// num.forEach((n,i ,num)=>{
//     console.log(n);
// })


// array methord filter--------------------------------------------------------------

let numbersVal = [42, 51, 24, 98, 65, 12]

numbersVal.filter(n => n % 2 === 0).map(n => n * 2)
    .forEach(n => {
        console.log(n);
    })
//set--------------------------------------------------------------------------------

//class in es-6
//collection
//all values are uinqe
//not have index val

// let num2= new  Set("gehanishanka");
// console.log(num2);

let num2= new  Set();
num2.add(3);
num2.add(4);
num2.add(3);
num2.add("gehan")
num2.add("ishanka")
console.log(num2);

num2.forEach(n=>{
    console.log(n);
})

console.log(num2.has(9));
console.log(num2.has(10));

//map------------------------------------------------------------------------------------
// map have key and value

let map = new Map();
map.set("FName","Gehan");
map.set("LName","Ishanka");
map.set("Age",36);

console.log(map);
console.log(map.keys());
console.log(map.values());

for(let [a,v] of map){
    console.log(a," : ",v);
}

// ----------------------------------------------------------------------------------------------------------------------

let x1
console.log(x1);