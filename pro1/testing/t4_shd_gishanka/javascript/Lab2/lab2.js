// 1.Create a list of Bank Objects (same kind of object you used in above lab, but in a array format)

let bankAccount = [
    {
        customer_name: "gayan sampath ",
        bank_name: "Peoples Bank",
        account_type: "savings",
        branch_name:"colombo",
        pan_card_number: 1223456786,
        date_of_creations: "2023/02/22",
        balance: 245
    },
    {
        customer_name: "mahesh gamage ",
        bank_name: "Bank of ceylon",
        account_type: "savings",
        branch_name:"Matara",
        pan_card_number: 1287689078,
        date_of_creations: "2023/02/22",
        balance: 350
    },
    {
        customer_name: "ruwan senanayake ",
        bank_name: "Commercial bank",
        account_type: "savings",
        branch_name:"Matara",
        pan_card_number: 3457689023,
        date_of_creations: "2023/02/22",
        balance: 100
    },
    {
        customer_name: "damindu thewarapperuma ",
        bank_name: "Bank of ceylon",
        account_type: "savings",
        branch_name:"Galle",
        pan_card_number: 1234056780,
        date_of_creations: "2023/02/22",
        balance: 123
    },
    {
        customer_name: "Asvika ",
        bank_name: "DBS Bank",
        account_type: "savings",
        branch_name:"Chennai",
        pan_card_number: 1234056780,
        date_of_creations: "2023/02/22",
        balance: 400
    }

];


// 2.Display the banks where balance is greater than 200
for (let n in bankAccount) {
    if (bankAccount[n].balance > 200) {
        console.log(bankAccount[n].bank_name)
    }
}

//3.deduct 10% of the Bank account balance, as part of monthly service fees
let NewBankAccountBalance;
function deduct(banckAccountBalance) {

    NewBankAccountBalance = (banckAccountBalance * 90) / 100;
    return NewBankAccountBalance;

}

//4.Display the banks where balance is greater than 200 and branch code is “Chennai
for (let n in bankAccount) {

    let val = deduct(bankAccount[n].balance)
    console.log("Old Bank Balance Rs:" + bankAccount[n].balance + "  New Bank Balance Rs:" + val)
    bankAccount[n].balance = val;

}
console.log(bankAccount)


for(let n in bankAccount){
    if(bankAccount[n].balance>200 && bankAccount[n].branch_name =="Chennai"){
        console.log("Bank balance Rs:"+bankAccount[n].balance)
    }
}

//5.Add a new Bank to the given array
bankAccount.push({
    customer_name: "Karthik ",
    bank_name: "Deutsche Bank India Chennai Branch",
    account_type: "savings",
    branch_name:"Chennai",
    pan_card_number: 1234056780,
    date_of_creations: "2023/02/22",
    balance: 10000
})
bankAccount.pop([5])
console.log(bankAccount)


//5.Delete a bank from the array (use splice operator)

console.log(bankAccount.splice(1))
console.log(bankAccount.length)
console.log(bankAccount)


//6.Calculate the total balance of all bank accounts

let totalBalanceOfallBanks=0;
for(let n in bankAccount){
    totalBalanceOfallBanks +=bankAccount[n].balance;
}
console.log("Total balance of all bank accounts Rs:"+totalBalanceOfallBanks);