// Create a collection of Customer by using
// Weak Map and Map Collection in JS
// Show Case the different feature set of the same.
let aditionalData=[100,400,100,500,700,100]
let customer = new Set(aditionalData);

customer.add("gehan");
customer.add("gayan");
customer.add("gehan");
customer.add("matara");
customer.add("matara")
customer.add(5);
customer.add(6);
customer.add(5);
customer.add(500);
customer.add("sandun");
customer.add("srilanka");

console.log(customer);



const myMap = new Map();
console.log(myMap);
myMap.set("info", { name: "Sam", age: 36 });
console.log(myMap);
console.log(myMap.get("info"));
console.log("check whether info is there or not - "
+ myMap.has("info"));



const myweakMap = new WeakMap();
console.log(myweakMap);

let obj = {};
myweakMap.set(obj, "hello everyone");
console.log(myweakMap);